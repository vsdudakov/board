#-*- encoding: utf-8 -*-
import fabric
import os

SITES_PATH = os.path.join('/home', 'vdudakov', 'board')

fabric.api.env.roledefs['production'] = ['revel-team.merann.ru']
fabric.api.env.user = 'vdudakov'


def production_env():
    fabric.api.env.project_root = SITES_PATH
    fabric.api.env.shell = '/bin/bash -c'
    fabric.api.env.python = os.path.join(SITES_PATH, 'env', 'bin', 'python')
    fabric.api.env.pip = os.path.join(SITES_PATH, 'env', 'bin', 'pip')
    fabric.api.env.uwsgi = os.path.join(SITES_PATH, 'env', 'bin', 'uwsgi')
    fabric.api.env.always_use_pty = False


def deploy(branch='master'):
    with fabric.api.cd(fabric.api.env.project_root):
        fabric.api.run('git fetch')
        fabric.api.run('git checkout %s' % branch)
        fabric.api.run('git reset --hard')
        fabric.api.run('git pull origin %s' % branch)

        fabric.api.run('{pip} install -r requirements.txt'.format(pip=fabric.api.env.pip))
        fabric.api.run('{python} manage.py migrate'.format(python=fabric.api.env.python))
        with fabric.api.warn_only():
            fabric.api.run("kill -9 $(ps aux | grep 0.0.0.0:8100 |grep -v grep | awk '{print $2}')")
        fabric.api.run('{python} manage.py runserver 0.0.0.0:8100'.format(python=fabric.api.env.python), shell=False, pty=True)


@fabric.api.roles('production')
def prod():
    production_env()
    deploy()