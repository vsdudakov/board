from django.contrib import admin

from tasks.models import Developer, Feature, Highlight, Task, TaskPoints


class DeveloperAdmin(admin.ModelAdmin):
    list_display = (
        'name',
    )
    search_fields = ('name',)


class FeatureAdmin(admin.ModelAdmin):

    class HighlightInline(admin.TabularInline):
        model = Highlight

    list_display = (
        'title',
        'start_date',
        'end_date',
        'story_points',
        'burned_story_points'
    )
    search_fields = ('title',)
    inlines = (HighlightInline,)
    filter_horizontal = ('email_developers',)


class TaskAdmin(admin.ModelAdmin):

    class TaskPointsInline(admin.TabularInline):
        model = TaskPoints

    list_display = (
        'id',
        'title',
        'description',
        'assigned_to',
        'status',
        'story_points',
        'burned_story_points'
    )
    list_filter = (
        'feature__title',
        'status',
        'assigned_to__name',
    )
    search_fields = ('title',)
    inlines = (TaskPointsInline,)


admin.site.register(Developer, DeveloperAdmin)
admin.site.register(Feature, FeatureAdmin)
admin.site.register(Task, TaskAdmin)