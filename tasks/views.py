import datetime

from django.shortcuts import redirect
from django.views.generic import TemplateView
from django.utils import timezone
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.conf import settings
from django.utils.decorators import method_decorator

from tasks.models import Feature, Task, Highlight
from tasks.forms import FilterForm


class FilterMixin(object):

    def get_form_kwargs(self):
        kwargs = {}
        data = {}
        data['feature'] = self.request.GET.get('feature', self.request.session.get('feature'))
        data['date'] = self.request.GET.get('date', self.request.session.get('date'))
        filter_form = FilterForm(data=data)
        kwargs['filter_form'] = filter_form
        if filter_form.is_valid():
            self.request.session['feature'] = filter_form.cleaned_data['feature'].id
            self.request.session['date'] = str(filter_form.cleaned_data['date'])
            kwargs['feature'] = filter_form.cleaned_data['feature']
            kwargs['date'] = filter_form.cleaned_data['date']
        else:
            messages.warning(self.request, 'Please select correct feature and date')
        return kwargs


class BoardView(FilterMixin, TemplateView):
    template_name = 'board.html'

    def get_context_data(self, **kwargs):
        kwargs = super(BoardView, self).get_context_data(**kwargs)
        kwargs.update(self.get_form_kwargs())
        feature = kwargs.get('feature')
        date = kwargs.get('date')
        if feature and date:
            kwargs['tasks'] = Task.objects.filter(feature=feature).order_by('status')
        return kwargs


class BoardProgressView(FilterMixin, TemplateView):
    template_name = 'progress.html'

    def get_burndown(self, feature, date):
        if not feature.end_date or not feature.start_date:
            return {}
        diff_date = feature.end_date - feature.start_date
        x_axis = [
            feature.start_date + datetime.timedelta(days=x) 
                for x in xrange(0, diff_date.days) 
        ]
        sp = float(feature.story_points())
        lxa = len(x_axis)
        y_axis_ideal = [round(sp - x * (sp/lxa), 1) for x in xrange(lxa)]
        y_axis_real = [sp - feature.burned_story_points(date=x) for x in x_axis if x <= date]
        return {
            'x_axis': x_axis,
            'y_axis_ideal': y_axis_ideal,
            'y_axis_real': y_axis_real
        }

    def send_email(self, data):
        feature = data.get('feature')
        date = data.get('date')
        if not feature or not date:
            messages.warning(self.request, 'Please select feature and date')
            return
        emails = list(feature.email_developers.all().filter(email__isnull=False).values_list('email', flat=True))
        subject = 'SOW (%s) standup minutes %s' % (feature.title, date)
        msg = render_to_string('email.html', data)
        if emails:
            send_mail(
                subject,
                msg,
                settings.EMAIL_FROM,
                emails,
                fail_silently=False,
                html_message=msg
            )

    def get_context_data(self, **kwargs):
        kwargs = super(BoardProgressView, self).get_context_data(**kwargs)
        kwargs.update(self.get_form_kwargs())
        kwargs['features'] = Feature.objects.all()
        feature = kwargs.get('feature')
        date = kwargs.get('date')
        if feature and date:
            kwargs['highlights'] = Highlight.objects.filter(feature=feature, date=date)
            kwargs['tasks_done'] = Task.objects.filter(feature=feature, status=Task.STATUS_RESOLVED, taskpoints__date=date)
            kwargs['tasks_inprogress'] = Task.objects.filter(feature=feature, status=Task.STATUS_IN_PROGRESS)
            kwargs['burndown'] = self.get_burndown(feature, date)
        return kwargs

    def post(self, request, *args, **kwargs):
        kwargs = self.get_context_data()
        kwargs['svg'] = request.POST.get('svg')
        self.send_email(kwargs)
        return redirect('/progress/')

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(BoardProgressView, self).dispatch(*args, **kwargs)

