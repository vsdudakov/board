from django import forms

from tasks.models import Feature


def form_fields_to_bootstrap(fields):
    for key in fields:
        field = fields[key]
        field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' form-control'})


class FilterForm(forms.Form):
    feature = forms.ModelChoiceField(queryset=Feature.objects.all())
    date = forms.DateField(label="Date (Y-m-d, e.g. 2017-01-01)")

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        form_fields_to_bootstrap(self.fields)  
