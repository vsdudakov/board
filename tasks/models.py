from __future__ import unicode_literals

from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import User


class Developer(models.Model):
    name = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.name


class Feature(models.Model):
    title = models.CharField(max_length=255, unique=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)

    email_developers = models.ManyToManyField(Developer, blank=True)

    def story_points(self):
        return Task.objects.filter(feature=self).aggregate(Sum('story_points')).get('story_points__sum') or 0

    def burned_story_points(self, date=None):
        qs = TaskPoints.objects.filter(task__feature=self)
        if date:
            qs = qs.filter(date__lte=date)
        return qs.aggregate(Sum('story_points')).get('story_points__sum') or 0

    def __unicode__(self):
        return u'%s' % self.title


class Highlight(models.Model):
    FLAG_GREEN = 0
    FLAG_YELLOW = 1
    FLAG_RED = 2
    FLAGS = (
        (FLAG_GREEN, 'Green'),
        (FLAG_YELLOW, 'Yellow'),
        (FLAG_RED, 'Red'),
    )
    feature = models.ForeignKey(Feature)
    date = models.DateField()
    flag = models.PositiveSmallIntegerField(choices=FLAGS)
    assigned_to = models.ForeignKey(Developer, null=True, blank=True)
    description = models.TextField()

    class Meta:
        ordering = ('date', 'flag',)


class Task(models.Model):
    STATUS_NEW = 0
    STATUS_IN_PROGRESS = 1
    STATUS_RESOLVED = 2
    STATUSES = (
        (STATUS_NEW, 'New'),
        (STATUS_IN_PROGRESS, 'In progress'),
        (STATUS_RESOLVED, 'Resolved'),
    )
    feature = models.ForeignKey(Feature)
    title = models.CharField(max_length=255, unique=True)
    description = models.TextField(null=True, blank=True)
    assigned_to = models.ForeignKey(Developer, null=True, blank=True)
    story_points = models.PositiveIntegerField(default=0)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=STATUS_NEW)

    def burned_story_points(self, date=None):
        qs = TaskPoints.objects.filter(task=self)
        if date:
            qs = qs.filter(date__lte=date)
        return qs.aggregate(Sum('story_points')).get('story_points__sum') or 0

    def __unicode__(self):
        return u'%s:%s' % (self.feature, self.title)


class TaskPoints(models.Model):
    task = models.ForeignKey(Task)
    date = models.DateField()
    story_points = models.FloatField()

    def __unicode__(self):
        return u'%s:%s:%s' % (self.task, self.date, self.story_points)

